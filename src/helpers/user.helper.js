import bcrypt from 'bcrypt';
import sms from '../functions/sms';
import mail from '../functions/mail';
import auth_code from '../mail-templates/auth-code';
import jsonwebtoken from 'jsonwebtoken';
require('dotenv').config();


async function signup({ name, email, password, phonenumber, context, info }) {

    const user = await context.db.mutation.createUser({
        data: {
            name,
            email,
            password: await bcrypt.hash(password, 10),
            phonenumber
        },
        info
    })
    if (user) {
        return await sendActivationKey(user, context, info);
    } else {
        throw new Error('Cannot send activation key');
    }
}

async function resendCode({ email, context, info }) {
    const user = await context.db.query.user({
        where: { email: email }, info
    })
    console.log(user);

    // if no user
    if (!user) {
        throw new Error('invalid details');
    } else {
        return await sendActivationKey(user, context, info);
    }

}
async function verify({ four_digit_code, user_id, context, info }) {
    let verifyy = await context.db.query.verificationCode(
        {
            where: { user_id: user_id, four_digit_code: four_digit_code }

        }

    );
    if (!verifyy) {
        throw new Error('Verification error');
    }

    var expTime = new Date(verifyy.time);

    var now = new Date();
    var diffMs = now - expTime; // milliseconds between now & Christmas
    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
    console.log(diffMins);

    if (diffMins < 60) {
        const user = await context.db.mutation.updateUser({
            data: {
                verified: true,
            },
            where: {
                id: user_id
            }, info
        })

        return {
            user,
            token: jsonwebtoken.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET, {
                expiresIn: '1y'
            })
        };
    } else {
        throw new Error('expired');
    }


}
async function login({ email, password, context, info }) {
    const user = await context.db.query.user({
        where: {
            email: email
        }
    });
    // if no user
    if (!user) {
        throw new Error('invalid_details');
    }

    //If not verified
    if (user.verified == false) {
        throw new Error('not_verified');
    }
    // Confirm Password
    const valid = await bcrypt.compare(password, user.password);

    if (!valid) {
        throw new Error('invalid_details');
    }

    // return json web token

    return {
        user,
        token: jsonwebtoken.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET, {
            expiresIn: '1d'
        })
    };


}

async function sendActivationKey(user, context, info) {
    return new Promise((resolve, reject) => {
        var activationKey = Math.floor(1000 + Math.random() * 9000);

        var time = new Date();
        const v = context.db.mutation.createVerificationCode({
            data: {
                user_id: user.id,
                four_digit_code: activationKey,
                time
            },
            info
        });
        console.log(v);

        try {
            var subject = "Activation Key"
            // sms(user, 'Hi,' + user.name + '.' + '. Use this activation key -' + activationKey);
            mail({ person: user, message: 'Hi,' + user.name + '.' + '. Use this activation key -' + activationKey, subject, html: auth_code(activationKey, user.name) })
            resolve({
                user,
                status: 'successful',
                message: 'Please verify with the code sent to you'
            });

        } catch (error) {
            reject(error)
        }


    });

}


export {
    signup,
    resendCode,
    verify,
    login
} 