import slugs from 'slug';
import Axios from 'axios';

function formatRestaurants(data) {
    // So we want to convert the time to JSON for better presentation, so we perform this little algorithm
    
    if (data[0] === undefined) {
        return data;
    }
	
	let newData = [];

	if (!(!("closing_time" in data[0]))) {
		newData = data.map(restaurant => {
			if (restaurant.closing_time) {
				restaurant.closing_time = JSON.parse(restaurant.closing_time)
			}

			return restaurant
		});
	}

	if (!(!("opening_time" in data[0]))) {
		newData = data.map(restaurant => {
			if (restaurant.opening_time) {
				restaurant.opening_time = JSON.parse(restaurant.opening_time)
			}

			return restaurant
		});
	}
	

	return newData
}

function formatRestaurant(data) {

    if (data === undefined) {
        return data;
    }

	if (!(!("closing_time" in data))) {
		data.closing_time = JSON.parse(data.closing_time)
	}

	if (!(!("opening_time" in data))) {
		data.opening_time = JSON.parse(data.opening_time)
	}

	return data
}

function createRestaurantFunc({
	name,
	image_url,
	opening_time,
	closing_time,
	phone_number,
	bank,
	account_number,
	context,
	info
}) {
	return new Promise((resolve, reject) => {
		Axios({
				url: 'https://api.paystack.co/subaccount',
				method: 'post',
				headers: {
					Authorization: 'Bearer ' + process.env.PS_SK,
					'Content-Type': 'application/json'
				},
				data: {
					business_name: name,
					settlement_bank: bank,
					account_number: account_number,
					percentage_charge: 0
				}
			})
			.then(async (response) => {
				

				try {
					resolve(context.db.mutation.createRestaurant({
						data: {
							name,
							image_url,
							opening_time: JSON.stringify(opening_time),
							closing_time: JSON.stringify(closing_time),
							phone_number,
							subaccount_code: response.data.data.subaccount_code,
							subaccount_id: response.data.data.id,
							slug: slugs(name.toLowerCase() + ' ' + Date.now()),
							bank,
							account_number
						},
					}, info))
				} catch (error) {
					console.log(error);
				}
			})
			.catch(function (error) {
				console.log(error.response.data);

				reject(error.response.data);
			});
	});
}

function updateRestaurantFunc({
	image_url,
	name,
	opening_time,
	closing_time,
	phone_number,
	slug,
	bank,
	account_number,
	context,
	info
}) {
	return new Promise(async (resolve, reject) => {
		let oldRestaurant
        
        try {
            oldRestaurant = await context.db.query.restaurant({
                where: {slug: slug}
            }, info)
            if (oldRestaurant === null) {
                throw new Error('Restaurant does not exist!');
            }
            
        } catch (error) {
            reject(error)
        }

		
		Axios({
				url: 'https://api.paystack.co/subaccount/' + oldRestaurant.subaccount_code,
				method: 'put',
				headers: {
					Authorization: 'Bearer ' + process.env.PS_SK,
					'Content-Type': 'application/json'
				},
				data: {
					business_name: name,
					settlement_bank: bank,
					account_number: account_number,
					percentage_charge: 0
				}
			})
			.then(async (response) => {

				try {

					// If name is different from old name update the slug
					if (oldRestaurant.name != name) {
						resolve(context.db.mutation.updateRestaurant({
							data: {
								name,
								image_url,
								opening_time: JSON.stringify(opening_time),
								closing_time: JSON.stringify(closing_time),
								phone_number,
								slug: slugs(name.toLowerCase() + ' ' + Date.now()),
								subaccount_code: response.data.data.subaccount_code,
								subaccount_id: response.data.data.id,
								bank,
								account_number
							},
							where: {
								slug
							}
						}, info))
					}

					// If not leave the slug as it is to maintain consistency
					
					resolve(context.db.mutation.updateRestaurant({
						data: {
							name,
							image_url,
							opening_time: JSON.stringify(opening_time),
							closing_time: JSON.stringify(closing_time),
							phone_number,
							subaccount_code: response.data.data.subaccount_code,
							subaccount_id: response.data.data.id,
							bank,
							account_number
						},
						where: {
							slug
						}
					}, info))
				} catch (error) {
                    reject(error);
				}
			})
			.catch(function (error) {
				reject('Status: ' + error.response.status + '. Message: ' + error.response.statusText);
			});
	});
	
}

function deleteRestaurantFunc({
    slug,
    context,
    info}) {
    
        return new Promise(async (resolve, reject) => {
			try {
				let oldRestaurant = await context.db.query.restaurant({
					where: {slug: slug}
				}, info)
				if (oldRestaurant === null) {
					throw new Error('Restaurant does not exist!');
				}

				resolve(context.db.mutation.updateRestaurant({
					data: {
						deleted: true,
						deletedAt: new Date()
					},
					where: {
						slug
					}
				}, info))
				
			} catch (error) {
				reject(error)
			}


		});
}

function restoreRestaurantFunc({
    slug,
    context,
    info}) {
    
        return new Promise(async (resolve, reject) => {
			try {
				let oldRestaurant = await context.db.query.restaurant({
					where: {slug: slug}
				}, info)
				if (oldRestaurant === null) {
					throw new Error('Restaurant does not exist!');
				}

				resolve(context.db.mutation.updateRestaurant({
					data: {
						deleted: false,
						deletedAt: null
					},
					where: {
						slug
					}
				}, info))
				
			} catch (error) {
				reject(error)
			}


		});
}

export {
    formatRestaurants,
    formatRestaurant,
    createRestaurantFunc,
	updateRestaurantFunc,
	deleteRestaurantFunc,
	restoreRestaurantFunc
}