import { GraphQLServer } from 'graphql-yoga';
const { Prisma } = require('prisma-binding');
import GraphQLJSON from 'graphql-type-json';
import merge from 'lodash/merge'; 
require('dotenv').config();

//Resolvers
 import userResolvers from './resolvers/user.resolver';
// import adminResolvers from './resolvers/admin.resolver';
import restaurantResolvers from './resolvers/restaurant.resolver';
import bankResolvers from './resolvers/bank.resolver';
import categoryResolvers from './resolvers/category.resolver';
import foodResolvers from './resolvers/food.resolver';

const resolveFunctions = {
	JSON: GraphQLJSON
};

const resolvers = merge(
	restaurantResolvers,
	resolveFunctions,
	// adminResolvers,
	categoryResolvers,
	foodResolvers,
	 userResolvers,
	bankResolvers
);

// 3
const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  },
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: process.env.endpoint,
      secret: process.env.secret,
      debug: process.env.debug,
    }),
  }),
})
server.start(() => console.log(`Server is running on http://localhost:4000`))
