
const FoodResolvers = {
	Query: {
		foods: (root, args, context, info) => {
			return context.db.query.foods({
				where: {deleted: false}
			}, info)
		},
		food: (root, args, context, info) => {
			return context.db.query.food({
				where: {id: args.id}
			}, info)
		}
	},
	Mutation: {
		createFood: (root, args, context, info) => {
			return context.db.mutation.createFood({
				data: {
					name: args.name,
					description: args.description,
					price: args.price,
					pack_price: args.pack_price,
					category: {connect: {slug: args.category_id}  },
				},
				}, info)
		},
		updateFood: (root, args, context, info) => {
			return context.db.mutation.updateFood({
				data: {
					name: args.name,
					description: args.description,
					price: args.price,
					pack_price: args.pack_price,
					category: {connect: {slug: args.category_id}  },
				},
				where: {
					id: args.id
				}
				}, info)
		},
		deleteFood: (root, args, context, info) => {
			return context.db.mutation.updateFood({
				data: {
					deleted: true,
					deletedAt: new Date()
				},
				where: {
					id: args.id
				}
				}, info)
		},
		restoreFood: (root, args, context, info) => {
			return context.db.mutation.updateFood({
				data: {
					deleted: false,
					deletedAt: null
				},
				where: {
					id: args.id
				}
				}, info)
		}
	}
};

export default FoodResolvers;
