import axios from 'axios';
require('dotenv').config();

const bankResolvers = {
	Query: {
		banks: () => {
			return new Promise((resolve, reject) => {
				axios
					.get('https://api.paystack.co/bank')
					.then(function(response) {
						// handle success
						resolve({
							status: response.data.status,
							message: response.data.message,
							data: response.data.data
						});
					})
					.catch(function(error) {
						// handle error
						console.log(error);
						reject(error);
					});
			});
		}
	}
};

module.exports = bankResolvers;
