import Deliverer from '../../models/deliverer.model';
import bcrypt from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
require('dotenv').config();

const delivererResolvers = {
	Query: {
		deliverers: async () => {
			const deliverers = await Deliverer.find();
			return deliverers;
		}
	},
	Mutation: {
		createDeliverer: async (_, { name, password, email, phonenumber }) => {
			const deliverer = await Deliverer.create({
				name,
				email,
				created_at: Date.now(),
				updated_at: Date.now(),
				password: await bcrypt.hash(password, 10),
				phonenumber
			});
			return deliverer;
		}
	}
};
export default delivererResolvers;
