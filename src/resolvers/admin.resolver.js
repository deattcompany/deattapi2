import bcrypt from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
import PasswordGenerator from 'generate-password';
import mail from '../functions/mail';
import admin_auth from '../mail-templates/admin-auth';
require('dotenv').config();

const adminResolvers = {
	Query: {
		admins: (root, args, context, info) => {
			return context.db.query.links({}, info)
		},
		admin: async (_, { id }) => {
			const admin = await Admin.findById({ _id: id });
			return admin;
		}
	},
	Mutation: {
		createAdmin: async (_, { email, name }) => {
			var generated_password = PasswordGenerator.generate({
				length: 14,
				numbers: true,
				uppercase: true,
				excludeSimilarCharacters: true
			});
			const admin = await Admin.create({
				name,
				email,
				created_at: Date.now(),
				updated_at: Date.now(),
				password: bcrypt.hashSync(generated_password, 8)
			});
			var subject = 'Account Details';
			mail({
				person: admin,
				message: 'Hi, ' + admin.name + '.' + '.Your password is  ' + generated_password,
				subject,
				html: admin_auth(generated_password, admin.name)
			});
			return {
				user: admin,
				status: 'succesful',
				message: 'Your password has been sent to your email address'
			};
		},
		updateAdmin: async (_, { id, name, email, password }) => {
			const oldAdmin = await Admin.findById({ _id: id });
			const admin = await Admin.findOneAndUpdate(
				{ _id: id },
				{
					$set: {
						email,
						password,
						name,
						created_at: oldAdmin.created_at,
						updated_at: Date.now()
					}
				},
				{ new: true }
			);
			return admin;
		},
		deleteAdmin: async (_, { id }) => {
			const admin = await Admin.findByIdAndRemove({ _id: id });
			return admin;
		},
		loginAdmin: async (_, { email, password }) => {
			const admin = await Admin.findOne({ email });
			// if no admin
			if (!admin) {
				throw new Error('Invalid credentials');
			}

			const valid = await bcrypt.compare(password, admin.password);

			// return json web token

			return {
				user: admin,
				token: jsonwebtoken.sign({ id: admin.id, email: admin.email }, process.env.JWT_SECRET, {
					expiresIn: '1d'
				})
			};
		}
	}
};
module.exports = adminResolvers;
