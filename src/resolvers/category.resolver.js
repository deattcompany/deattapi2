const categoryResolvers = {
	Query: {
		categories: (root, args, context, info) => {
			return context.db.query.categories({
				where: {deleted: false}
			}, info)
		},
		
		category: (root, args, context, info) => {
			return context.db.query.category({
				where: {id: args.id}
			}, info)
		}
	},
	Mutation: {
		createCategory: (root, args, context, info) => {
			return context.db.mutation.createCategory({
				data: {
				  name: args.name,
				  restaurant: {connect: {slug: args.restaurant_slug} },
				},
			  }, info)
		},
		updateCategory: (root, args, context, info) => {
			return context.db.mutation.updateCategory({
				data: {
				  name: args.name
				},
				where: {
					id: args.id
				}
				}, info)
		},
		deleteCategory: (root, args, context, info) => {
			return context.db.mutation.updateCategory({
				data: {
					deleted: true,
					deletedAt: new Date()
				},
				where: {
					id: args.id
				}
				}, info)
		},
		restoreCategory: (root, args, context, info) => {
			return context.db.mutation.updateCategory({
				data: {
					deleted: false,
					deletedAt: null
				},
				where: {
					id: args.id
				}
				}, info)
		}
	}
};

export default categoryResolvers;
