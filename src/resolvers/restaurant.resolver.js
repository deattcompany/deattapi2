
import {
    formatRestaurants,
    formatRestaurant,
    createRestaurantFunc,
    updateRestaurantFunc,
	deleteRestaurantFunc,
	restoreRestaurantFunc
} from '../helpers/restaurant.helper';

require('dotenv').config();

const restaurantResolvers = {
	Query: {
		restaurants: async (root, args, context, info) => {

			return await context.db.query.restaurants({
				orderBy: args.orderBy,
				where: {deleted: false}
			}, info);

			// So we want to convert the time to JSON for better presentation, so we perform this little algorithm that can be made better by Wunmi
			// return formatRestaurants(data);
		},
		restaurant: async (root, args, context, info) => {
			let data = await context.db.query.restaurant({
				where: {slug: args.slug}
			}, info)

			// So we want to convert the time to JSON for better presentation, so we perform this little algorithm that can be made better by Wunmi
			return formatRestaurant(data);
		}
	},
	Mutation: {
		createRestaurant: async (root, args, context, info) => {
			const {name,
				image_url,
				opening_time,
				closing_time,
				phone_number,
				bank,
				account_number,
				} = args;
				

			const res = await createRestaurantFunc({
				name,
				image_url,
				opening_time,
				closing_time,
				phone_number,
				bank,
				account_number,
				context,
				info
			});
			// So we want to convert the time to JSON for better presentation, so we perform this little algorithm that can be made better by Wunmi

			return formatRestaurant(res);
		},
		updateRestaurant: async (root, args, context, info) => {
			const {name,
				image_url,
				opening_time,
				closing_time,
				phone_number,
				bank,
				slug,
				account_number,
				} = args;
				

			const res = await updateRestaurantFunc({
				name,
				image_url,
				slug,
				opening_time,
				closing_time,
				phone_number,
				bank,
				account_number,
				context,
				info
			});
			
			return formatRestaurant(res);
		},
		deleteRestaurant: async (root, args, context, info) => {
			const {slug} = args;
				

			const res = await deleteRestaurantFunc({
				slug,
				context,
				info
			});
			

			return formatRestaurant(res);
		},
		restoreRestaurant: async (root, args, context, info) => {
			const {slug} = args;

			// This mutation restores a deleted restaurant
				

			const res = await restoreRestaurantFunc({
				slug,
				context,
				info
			});
			

			return formatRestaurant(res);
		}
	}
};


export default restaurantResolvers;