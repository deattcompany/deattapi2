
import { signup, resendCode, verify, login } from '../helpers/user.helper';

const userResolvers = {
	Query: {
		users: async (root, args, context, info) => {
			return await context.db.query.users();
		},
		user: async (root, args, context, info) => {
			return await context.db.query.user({
				where: { id: args.id }
			}, info)
		}
	},
	Mutation: {
		signup: async (root, args, context, info) => {
			const { name, email, password, phonenumber } = args
			return await signup({
				name,
				email,
				password,
				phonenumber,
				context,
				info
			});

		},

		resendCode: async (root, args, context, info) => {
			const { email } = args;
			return await resendCode({
				email,
				context,
				info
			})

		},

		verify: async (root, args, context, info) => {
			const { four_digit_code, user_id } = args;
			return await verify({
				four_digit_code,
				user_id,
				context,
				info
			})

		},

		login: async (root, args, context, info) => {
			const { email, password } = args;
			return await login({
				email,
				password,
				context,
				info
			})
		}
	}


};

export default userResolvers;
