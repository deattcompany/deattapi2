const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
require('dotenv').config();

export default function sms(user, message) {
	var url =
		'http://portal.bulksmsnigeria.net/api/?username=' +
		process.env.SMS_USERNAME +
		'&password=' +
		process.env.SMS_PASSWORD +
		'&sender=' +
		process.env.SMS_SENDER +
		'&message=' +
		message +
		'&mobiles=' +
		user.phonenumber;
	var request = new XMLHttpRequest();
	request.open('GET', url);
	request.onload = function() {
		console.log(request.responseText);
	};
	request.send();
}
