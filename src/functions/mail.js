var mailgun = require("mailgun-js");
require('dotenv').config();

var mailgun = require('mailgun-js')({ apiKey: process.env.MG_API_KEY, domain: process.env.MG_DOMAIN });

export default function mail({person,message,subject, html=''}) {
    var data = {
        from: 'Deatt <noreply@deatt.com>',
        to: person.email,
        subject: subject,
        text: message,
        html: html
    };

    mailgun.messages().send(data, function (error, body) {
        console.log(body);
    });
}

 

